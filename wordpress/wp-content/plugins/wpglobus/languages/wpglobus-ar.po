# WPGlobus
# Copyright (C) YEAR TIV.NET INC.
# This file is distributed under the same license as the WPGlobus package.
#
#
# Translators:
# Omar Anwar <omaraglan91@yahoo.com>, 2017
msgid ""
msgstr ""
"Project-Id-Version: WPGlobus Multilingual\n"
"Report-Msgid-Bugs-To: support@wpglobus.com\n"
"POT-Creation-Date: 2018-01-19 16:58-0500\n"
"PO-Revision-Date: 2018-01-06 04:07+0000\n"
"Last-Translator: Gregory Karpinsky <gregory@tiv.net>\n"
"Language-Team: Arabic (http://www.transifex.com/wp-translations/wpglobus-"
"multilingual/language/ar/)\n"
"Language: ar\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 "
"&& n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;\n"

#. / Plugin Name
#. / Plugin Author
#: wpglobus.php:48 wpglobus.php:54
#: includes/admin/class-wpglobus-customize-options.php:545
msgid "WPGlobus"
msgstr "WPGlobus"

#. / Plugin Description
#: wpglobus.php:50
msgid ""
"A WordPress Globalization / Multilingual Plugin. Posts, pages, menus, "
"widgets and even custom fields - in multiple languages!"
msgstr ""

#. / Plugin URI
#: wpglobus.php:52
msgid "https://github.com/WPGlobus/WPGlobus"
msgstr "https://github.com/WPGlobus/WPGlobus"

#. / Plugin Author URI
#: wpglobus.php:56
msgid "https://wpglobus.com/"
msgstr "https://wpglobus.com/"

#: includes/admin/central/class-wpglobus-admin-central.php:122
#: includes/admin/class-wpglobus-about.php:45
#: includes/admin/class-wpglobus-about.php:120
#: includes/admin/class-wpglobus-clean.php:596
#: includes/admin/helpdesk/wpglobus-admin-helpdesk-page.php:37
msgid "Guide"
msgstr "الدليل"

#: includes/admin/central/class-wpglobus-admin-central.php:142
#: includes/admin/helpdesk/class-wpglobus-admin-helpdesk.php:53
msgid "WPGlobus Help Desk"
msgstr "WPGlobus دائرة المساعدة"

#: includes/admin/central/class-wpglobus-admin-central.php:157
#: includes/admin/class-wpglobus-about.php:39
#: includes/admin/class-wpglobus-admin-menu.php:28
#: includes/admin/class-wpglobus-clean.php:602
#: includes/admin/class-wpglobus-customize-options.php:1254
#: includes/admin/class-wpglobus-customize-options.php:1286
#: includes/admin/helpdesk/wpglobus-admin-helpdesk-page.php:31
msgid "Add-ons"
msgstr "الإضافات"

#: includes/admin/class-wpglobus-about.php:26
msgid "Quick Start"
msgstr "بداية سريعة"

#. / Do not translate
#: includes/admin/class-wpglobus-about.php:33
#: includes/admin/class-wpglobus-clean.php:599
#: includes/admin/helpdesk/wpglobus-admin-helpdesk-page.php:26
msgid "Settings"
msgstr "الإعدادت"

#: includes/admin/class-wpglobus-about.php:51
#: includes/admin/helpdesk/wpglobus-admin-helpdesk-page.php:43
msgid "FAQ"
msgstr "أسئلة متداولة"

#: includes/admin/class-wpglobus-about.php:62
msgid ""
"Attention: the Multibyte String PHP extension (`mbstring`) is not loaded!"
msgstr "إنتباه: سلسلة البايت المتعدد إضافة PHP (mbstring) غير مُحمل!"

#: includes/admin/class-wpglobus-about.php:63
msgid ""
"The mbstring extension is required for the full UTF-8 compatibility and "
"better performance. Without it, some parts of WordPress and WPGlobus may "
"function incorrectly. Please contact your hosting company or systems "
"administrator."
msgstr ""
"إضافة الـ\"mbstring\" مطلوبة من أجل دعم UTF-8 الكامل و أداء أفضل. من غيرها، "
"بعض الأجزاء من  WordPress و WPGlobus ربما لن تعمل جيداً. من فضلك إتصل بشركة "
"الإستضافة الخاصة بك أو مدير النظام."

#: includes/admin/class-wpglobus-about.php:90
msgid "Go to WPGlobus Settings"
msgstr "إذهب إلي إعدادت WPGlobus "

#: includes/admin/class-wpglobus-about.php:101
msgid "Easy as 1-2-3:"
msgstr "سهل مثل 1-2-3:"

#: includes/admin/class-wpglobus-about.php:104
msgid "Go to WPGlobus admin menu and choose the countries / languages;"
msgstr "إذهب إلي قائمة المدير الخاصة بـWPGlobus وإختر البلاد/اللغة;"

#: includes/admin/class-wpglobus-about.php:105
msgid ""
"Enter the translations to the posts, pages, categories, tags and menus using "
"a clean and simple interface."
msgstr ""
"أدخل الترجمة إلي المنشورات، الصفح، الأقسام، الإشارات و القوائم مستخدماً واجهة "
"بسيطة ونظيفة."

#: includes/admin/class-wpglobus-about.php:106
msgid ""
"Switch languages at the front-end using a drop-down menu with language names "
"and country flags."
msgstr ""
"غير اللغة في الواجهة بإستخدام القائمة المنسدلة مع أسماء اللغات و أعلام الدول."

#: includes/admin/class-wpglobus-about.php:114
msgid "Links:"
msgstr "الراوبط:"

#: includes/admin/class-wpglobus-about.php:122
msgid "FAQs"
msgstr "الأسئلة المتكررة"

#: includes/admin/class-wpglobus-about.php:124
msgid "Contact Us"
msgstr "إتصل بنا"

#: includes/admin/class-wpglobus-about.php:126
msgid "Please give us 5 stars!"
msgstr "من فضلك أعطنا 5 نجوم!"

#: includes/admin/class-wpglobus-about.php:136
msgid "WPGlobus does not translate texts automatically!"
msgstr "WPGlobus  لا يُترجم النصوص تلقائياً!"

#: includes/admin/class-wpglobus-about.php:139
msgid ""
"There are many translation companies and individual translators who can help "
"you write and proofread the texts."
msgstr ""
"هناك شركات ترجمة كثيرة و مترجمين مستقلين يمكنهم مساعدتك في ترجمة وتصحيح "
"الأخطاء في نصوصك."

#: includes/admin/class-wpglobus-about.php:140
msgid ""
"When you choose a translator, please look at their native language, country "
"of residence, specialization and knowledge of WordPress."
msgstr ""
"حينما تختر مترجماً، من فضلك أنظر إلي لغتهم الأم، بلد المعيشة، التخصص ومعرفتة "
"بـ WordPress."

#. / translators: %s are used to insert HTML link. Keep them in place.
#: includes/admin/class-wpglobus-about.php:146
#, php-format
msgid ""
"We are planning to maintain a %s list of translators %s on the WPGlobus "
"website. This is not an endorsement, just a courtesy. Please contact them "
"directly and let us know how did it work for you!"
msgstr ""
"نحن نخطط لإبقاء %sقائمة بالمترجمين %s في موقع WPGlobus. هذا ليس إقرار، هي "
"فقط مجاملة. من فضلك تواصل معهم وأعلمنا ماذا حدث معك!"

#: includes/admin/class-wpglobus-about.php:157
msgid "Important notes:"
msgstr "ملاحظات هامة:"

#: includes/admin/class-wpglobus-about.php:162
msgid ""
"WPGlobus only supports the localization URLs in the form of <code>example."
"com/xx/page/</code>. We do not plan to support subdomains <code>xx.example."
"com</code> and language queries <code>example.com?lang=xx</code>."
msgstr ""
"WPGlobus يدعم فقط ترجمة الروابط في الصيغة<code>example.com/xx/page/</code>. "
"نحن لا ننوي دعم النطاقات الفرعية<code>xx.example.com</code> وعلامات اللغة "
"<code>example.com?lang=xx</code>."

#: includes/admin/class-wpglobus-about.php:165
msgid "Some themes and plugins are <strong>not multilingual-ready</strong>."
msgstr "بعض الثيمات و الإضافات هي<strong>ليس لديها لغات متنوعة</strong>."

#: includes/admin/class-wpglobus-about.php:166
msgid ""
"They might display some texts with no translation, or with all languages "
"mixed together."
msgstr "هذا ربما يظهر بعض الكتابات بدون ترجمة، أو مع كل اللغات مختلطة."

#. / translators: %s are used to insert HTML link. Keep them in place.
#: includes/admin/class-wpglobus-about.php:170
#, php-format
msgid ""
"Please contact the theme / plugin author. If they are unable to assist, "
"consider %s hiring the WPGlobus Team %s to write a custom code for you."
msgstr ""
"من فضلك تواصل مع مالك الإضافة أو الثيم. إذا كانوا لا يستطيعوا المساعدة، فكر "
"في %s تأجير فريق WPGlobus%s لكي يكتب كود مخصص لك."

#: includes/admin/class-wpglobus-admin-menu.php:29
msgid "View free and premium WPGlobus extensions"
msgstr "أظهر إضافات WPGlobus المجانية والممتازة"

#: includes/admin/class-wpglobus-admin-page.php:32
#: includes/admin/class-wpglobus-clean.php:581
msgid "Multilingual Everything!"
msgstr "عدد لغات كل شئ!"

#: includes/admin/class-wpglobus-admin-page.php:34
#: includes/admin/class-wpglobus-clean.php:584
msgid ""
"WPGlobus is a family of WordPress plugins assisting you in making "
"multilingual WordPress blogs and sites."
msgstr ""
"WPGlobus هي عائلة من إضافات ووردبريس يساعد في جعل المواقع و المدونات متعددة "
"اللغة."

#: includes/admin/class-wpglobus-clean.php:232
msgid "Remove the WPGlobus settings (not recommended)"
msgstr "إمسح إعدادات WPGlobus (غير مُفضل)"

#: includes/admin/class-wpglobus-clean.php:591
msgid "Clean-up Tool"
msgstr "أداة التنظيف"

#: includes/admin/class-wpglobus-clean.php:606
msgid "Support"
msgstr "الدعم الفني"

#: includes/admin/class-wpglobus-clean.php:611
msgid ""
"WARNING: this operation is non-reversible. It is strongly recommended that "
"you backup your database before proceeding."
msgstr ""
"تحذير: العملية لا يمكن الرجوع عنها. يُفضل بشدة أن تعمل نسخة إحتياطية لقاعدة "
"البيانات قبل المتابعة."

#: includes/admin/class-wpglobus-clean.php:615
msgid ""
"This tool should be used only if you plan to completely uninstall WPGlobus. "
"By running it, you will remove ALL translations you have entered to your "
"post, pages, etc., keeping only the MAIN language texts. Please make sure "
"that all entries have some content in the main language. Otherwise, you "
"might end up with empty titles, no content, no excerpts, blank comments and "
"so on."
msgstr ""

#. / translators: %1$s - language name, %1$s - language code. Do not remove.
#: includes/admin/class-wpglobus-clean.php:622
#, php-format
msgid ""
"The main language is currently set to %1$s (%2$s). ALL TEXTS THAT ARE NOT IN "
"%1$s WILL BE DELETED! To change the main language, please go to Settings."
msgstr ""

#: includes/admin/class-wpglobus-clean.php:631
msgid "You are about to clean the content of the following database tables:"
msgstr ""

#: includes/admin/class-wpglobus-clean.php:639
msgid "The operations log"
msgstr ""

#: includes/admin/class-wpglobus-clean.php:642
msgid ""
"We are going to write a detailed log of all the database changes performed. "
"It should help in the case you need to restore something important. The log "
"will be written to the file:"
msgstr ""

#: includes/admin/class-wpglobus-clean.php:652
msgid ""
"Uncheck if you do not want to write the operations log (we recommend to keep "
"it checked)"
msgstr ""

#: includes/admin/class-wpglobus-clean.php:657
msgid "You have been warned..."
msgstr "لقد تم تحذيرك..."

#: includes/admin/class-wpglobus-clean.php:659
msgid "Please confirm by checking the box below:"
msgstr ""

#: includes/admin/class-wpglobus-clean.php:661
msgid ""
"I have read and understood everything written on this page. I am aware that "
"by using this tool I may loose some content of my website. I have made a "
"database backup and know how to restore it if necessary. I am fully "
"responsible for the results."
msgstr ""

#: includes/admin/class-wpglobus-clean.php:664
msgid "YES, I CONFIRM"
msgstr "أجل، أنا أؤكد"

#: includes/admin/class-wpglobus-clean.php:668
msgid "Process with the Clean-up"
msgstr ""

#: includes/admin/class-wpglobus-customize-options.php:73
#: includes/admin/class-wpglobus-customize-options.php:100
#: includes/admin/class-wpglobus-customize-options.php:145
#: includes/admin/class-wpglobus-customize-options.php:183
msgid "Default"
msgstr "الأفتراضي"

#: includes/admin/class-wpglobus-customize-options.php:264
msgid "Section"
msgstr "قسم"

#: includes/admin/class-wpglobus-customize-options.php:277
msgid "Show all sections"
msgstr ""

#: includes/admin/class-wpglobus-customize-options.php:279
msgid "Save &amp; Reload"
msgstr ""

#: includes/admin/class-wpglobus-customize-options.php:596
msgid ""
"You need to update WordPress to 4.5 or later to get Fields Settings section"
msgstr ""

#: includes/admin/class-wpglobus-customize-options.php:602
msgid "Fields Settings"
msgstr ""

#: includes/admin/class-wpglobus-customize-options.php:647
msgid "WPGlobus Settings"
msgstr "إعدادات WPGlobus "

#: includes/admin/class-wpglobus-customize-options.php:715
#, fuzzy
msgid "Help"
msgstr "طاولة المساعدة"

#: includes/admin/class-wpglobus-customize-options.php:737
#: includes/options/class-wpglobus-options.php:339
msgid "Languages"
msgstr "اللغات"

#: includes/admin/class-wpglobus-customize-options.php:757
#: includes/options/class-wpglobus-options.php:354
msgid "Enabled Languages"
msgstr "اللغات المفعلة"

#: includes/admin/class-wpglobus-customize-options.php:759
#: includes/options/class-wpglobus-options.php:356
msgid "These languages are currently enabled on your site."
msgstr "هذه اللغات هي حالياً مفغلة علي موقعك."

#: includes/admin/class-wpglobus-customize-options.php:787
#, fuzzy
msgid "Choose a language you would like to enable."
msgstr "إختر اللغة التي تريد أن تُفعلها. <br> إضغط زر [إحفظ & أنشر] لكي تؤكد."

#: includes/admin/class-wpglobus-customize-options.php:789
#, fuzzy
msgid "Press the [Save & Publish] button to confirm."
msgstr "إختر اللغة التي تريد أن تُفعلها. <br> إضغط زر [إحفظ & أنشر] لكي تؤكد."

#. / translators: %1$s and %2$s - placeholders to insert HTML link around 'here'
#: includes/admin/class-wpglobus-customize-options.php:793
#: includes/options/class-wpglobus-options.php:334
#, php-format
msgid "or Add new Language %1$s here %2$s"
msgstr "أو أضف لغة جديدة %1$s هنا %2$s"

#: includes/admin/class-wpglobus-customize-options.php:805
#: includes/options/class-wpglobus-options.php:364
msgid "Add Languages"
msgstr "أضف لغة"

#: includes/admin/class-wpglobus-customize-options.php:825
#: includes/options/class-wpglobus-options.php:374
msgid "Language Selector Mode"
msgstr "وضع محدد اللغة"

#: includes/admin/class-wpglobus-customize-options.php:830
#: includes/options/class-wpglobus-options.php:383
msgid "Two-letter Code with flag (en, ru, it, etc.)"
msgstr ""

#: includes/admin/class-wpglobus-customize-options.php:831
#: includes/options/class-wpglobus-options.php:384
msgid "Full Name (English, Russian, Italian, etc.)"
msgstr ""

#: includes/admin/class-wpglobus-customize-options.php:833
#: includes/options/class-wpglobus-options.php:386
msgid "Full Name with flag (English, Russian, Italian, etc.)"
msgstr ""

#: includes/admin/class-wpglobus-customize-options.php:834
#: includes/options/class-wpglobus-options.php:387
msgid "Flags only"
msgstr "أعلام فقط"

#: includes/admin/class-wpglobus-customize-options.php:836
#: includes/options/class-wpglobus-options.php:377
msgid ""
"Choose the way language name and country flag are shown in the drop-down menu"
msgstr "إختر الطريقة بحيث كيفية ظهور أسم اللغة والعلم في القائمة المنسدلة"

#: includes/admin/class-wpglobus-customize-options.php:871
msgid "select navigation menu"
msgstr ""

#: includes/admin/class-wpglobus-customize-options.php:880
#: includes/admin/class-wpglobus-customize-options.php:907
#: includes/options/class-wpglobus-options.php:395
msgid "Language Selector Menu"
msgstr "قائمة إختيار اللغة"

#: includes/admin/class-wpglobus-customize-options.php:890
msgid "No menus have been created yet. Create some."
msgstr "لا قوائم قد أونشئت بعد، أنشئ البعض."

#: includes/admin/class-wpglobus-customize-options.php:891
#: includes/admin/class-wpglobus-customize-options.php:912
#: includes/options/class-wpglobus-options.php:398
msgid "Choose the navigation menu where the language selector will be shown"
msgstr ""

#: includes/admin/class-wpglobus-customize-options.php:932
#: includes/options/class-wpglobus-options.php:409
msgid "\"All Pages\" menus Language selector"
msgstr ""

#: includes/admin/class-wpglobus-customize-options.php:935
#: includes/options/class-wpglobus-options.php:411
msgid ""
"Adds language selector to the menus that automatically list all existing "
"pages (using `wp_list_pages`)"
msgstr ""

#: includes/admin/class-wpglobus-customize-options.php:952
#: includes/options/class-wpglobus-options.php:421
msgid "Custom CSS"
msgstr "نمط مخصص"

#: includes/admin/class-wpglobus-customize-options.php:956
#: includes/options/class-wpglobus-options.php:425
msgid ""
"Here you can enter the CSS rules to adjust the language selector menu for "
"your theme. Look at the examples in the `style-samples.css` file."
msgstr ""

#: includes/admin/class-wpglobus-customize-options.php:975
#: includes/options/class-wpglobus-options.php:517
msgid "Post types"
msgstr "نوع المنشورات"

#: includes/admin/class-wpglobus-customize-options.php:1084
#: includes/options/class-wpglobus-options.php:477
msgid "Uncheck to disable WPGlobus"
msgstr ""

#: includes/admin/class-wpglobus-customize-options.php:1118
#: includes/options/class-wpglobus-options.php:555
msgid "Redirect"
msgstr "إعادة توجية"

#: includes/admin/class-wpglobus-customize-options.php:1143
#: includes/options/class-wpglobus-options.php:547
msgid "Choose the language automatically, based on:"
msgstr ""

#: includes/admin/class-wpglobus-customize-options.php:1145
#: includes/options/class-wpglobus-options.php:538
msgid "Preferred language set in the browser"
msgstr ""

#: includes/admin/class-wpglobus-customize-options.php:1146
#: includes/options/class-wpglobus-options.php:530
msgid ""
"When a user comes to the site for the first time, try to find the best "
"matching language version of the page."
msgstr ""

#: includes/admin/class-wpglobus-customize-options.php:1177
msgid ""
"To add a Custom JS Code in Customizer, you need to upgrade WordPress to "
"version 4.9 or later."
msgstr ""

#: includes/admin/class-wpglobus-customize-options.php:1179
msgid "With your version of WordPress, please use the"
msgstr ""

#: includes/admin/class-wpglobus-customize-options.php:1181
#, fuzzy
msgid "WPGlobus Settings page"
msgstr "إعدادات WPGlobus "

#: includes/admin/class-wpglobus-customize-options.php:1185
#: includes/admin/class-wpglobus-customize-options.php:1207
#: includes/admin/class-wpglobus-customize-options.php:1227
#: includes/options/class-wpglobus-options.php:433
msgid "Custom JS Code"
msgstr ""

#: includes/admin/class-wpglobus-customize-options.php:1225
#: includes/admin/class-wpglobus-customize-options.php:1269
#: includes/class-wpglobus-widget.php:236
msgid "Title"
msgstr "المسمى الوظيفي"

#: includes/admin/class-wpglobus-customize-options.php:1228
#: includes/options/class-wpglobus-options.php:438
msgid "(Paste your JS code here.)"
msgstr ""

#: includes/admin/class-wpglobus-customize-options.php:1270
msgid "Label"
msgstr "عنوان"

#: includes/admin/class-wpglobus-customize-options.php:1274
msgid "Description"
msgstr " الوصف"

#: includes/admin/class-wpglobus-customize-options.php:1338
msgid ""
"Here you can specify which fields should be considered multilingual by "
"WPGlobus. To exclude a field, uncheck it and then press the button below."
msgstr ""

#: includes/admin/class-wpglobus-customize-options.php:1344
#: includes/options/class-wpglobus-options.php:172
msgid "Thank you for installing WPGlobus!"
msgstr "شكراً لك لتثبيت WPGlobus !"

#: includes/admin/class-wpglobus-customize-options.php:1348
#: includes/options/class-wpglobus-options.php:177
msgid "Read About WPGlobus"
msgstr "إقرأ عن WPGlobus "

#: includes/admin/class-wpglobus-customize-options.php:1351
#: includes/options/class-wpglobus-options.php:180
msgid ""
"Click the <strong>[Languages]</strong> tab at the left to setup the options."
msgstr ""

#: includes/admin/class-wpglobus-customize-options.php:1356
#: includes/options/class-wpglobus-options.php:185
msgid ""
"Should you have any questions or comments, please do not hesitate to contact "
"us."
msgstr ""

#: includes/admin/class-wpglobus-customize-options.php:1360
#: includes/admin/helpdesk/wpglobus-admin-helpdesk-page.php:69
#: includes/options/class-wpglobus-options.php:189
msgid "Sincerely Yours,"
msgstr ""

#: includes/admin/class-wpglobus-customize-options.php:1362
#: includes/admin/helpdesk/wpglobus-admin-helpdesk-page.php:70
#: includes/options/class-wpglobus-options.php:191
#: includes/options/wpglobus-options-header.php:29
msgid "The WPGlobus Team"
msgstr ""

#. / translators: %?$s: HTML codes for hyperlink. Do not remove.
#: includes/admin/class-wpglobus-customize-options.php:1381
#: includes/options/class-wpglobus-options.php:216
#, php-format
msgid ""
"We would hate to see you go. If something goes wrong, do not uninstall "
"WPGlobus yet. Please %1$stalk to us%2$s and let us help!"
msgstr ""

#: includes/admin/class-wpglobus-customize-options.php:1388
#: includes/options/class-wpglobus-options.php:223
msgid ""
"Please note that if you deactivate WPGlobus, your site will show all the "
"languages together, mixed up. You will need to remove all translations, "
"keeping only one language."
msgstr ""

#. / translators: %s: link to the Clean-up Tool
#: includes/admin/class-wpglobus-customize-options.php:1393
#: includes/options/class-wpglobus-options.php:228
#, php-format
msgid ""
"If there are just a few places, you should edit them manually. To "
"automatically remove all translations at once, you can use the %s. WARNING: "
"The clean-up operation is irreversible, so use it only if you need to "
"completely uninstall WPGlobus."
msgstr ""

#. / translators: %?$s: HTML codes for hyperlink. Do not remove.
#: includes/admin/class-wpglobus-customize-options.php:1396
#: includes/options/class-wpglobus-options.php:231
#, php-format
msgid "%1$sClean-up Tool%2$s"
msgstr ""

#. / translators: %s: name of current theme
#: includes/admin/class-wpglobus-customize-options.php:1408
#, php-format
msgid "Sorry, WPGlobus customizer doesn't support current theme %s."
msgstr ""

#. / translators: %?$s: HTML codes for hyperlink. Do not remove.
#: includes/admin/class-wpglobus-customize-options.php:1414
#, php-format
msgid "Please use %1$sWPGlobus options page%2$s instead."
msgstr ""

#: includes/admin/class-wpglobus-customize-options.php:1478
msgid "Expand/Shrink"
msgstr ""

#: includes/admin/class-wpglobus-dashboard-news.php:43
#: includes/admin/class-wpglobus-dashboard-news.php:59
msgid "WPGlobus News"
msgstr "أخبار WPGlobus "

#: includes/admin/class-wpglobus-language-edit.php:192
#: includes/admin/class-wpglobus-language-edit.php:197
msgid "Options updated"
msgstr "الإعدادات حُدثت"

#: includes/admin/class-wpglobus-language-edit.php:272
msgid "Please enter a language code!"
msgstr "من فضلك أدخل كود لغة!"

#: includes/admin/class-wpglobus-language-edit.php:276
msgid "Language code already exists!"
msgstr "كود اللغة موجود مسبقاً!"

#: includes/admin/class-wpglobus-language-edit.php:280
msgid "Please specify the language flag!"
msgstr "من فضلك حدد علم اللغة!"

#: includes/admin/class-wpglobus-language-edit.php:284
msgid "Please enter the language name!"
msgstr "من فضلك أدخل أسم اللغة!"

#: includes/admin/class-wpglobus-language-edit.php:288
msgid "Please enter the language name in English!"
msgstr "من فضلك أدخل اللغة في الإنجليزية!"

#: includes/admin/class-wpglobus-language-edit.php:292
msgid "Please enter the locale!"
msgstr "من فضلك أدخل اللفة!"

#: includes/admin/class-wpglobus-language-edit.php:349
msgid "Edit Language"
msgstr "تعديل اللغة"

#: includes/admin/class-wpglobus-language-edit.php:351
msgid "Are you sure you want to delete?"
msgstr "هل أنت متأكد من المسح؟"

#: includes/admin/class-wpglobus-language-edit.php:354
msgid "Add Language"
msgstr "اضافة لغة"

#: includes/admin/class-wpglobus-language-edit.php:384
msgid "Language Code"
msgstr "كود اللغة"

#: includes/admin/class-wpglobus-language-edit.php:391
msgid ""
"2-Letter ISO Language Code for the Language you want to insert. (Example: en)"
msgstr ""

#: includes/admin/class-wpglobus-language-edit.php:396
msgid "Language flag"
msgstr "علم اللغة"

#: includes/admin/class-wpglobus-language-edit.php:412
msgid "Name"
msgstr "الأسم"

#: includes/admin/class-wpglobus-language-edit.php:417
msgid ""
"The name of the language in its native alphabet. (Examples: English, Русский)"
msgstr "أسم اللغة في اللغة الأم.(أمثلة: العربية، English)"

#: includes/admin/class-wpglobus-language-edit.php:422
msgid "Name in English"
msgstr "الأسم في الإنجليزية"

#: includes/admin/class-wpglobus-language-edit.php:427
msgid "The name of the language in English"
msgstr "أسم اللغة في الإنجليزية"

#: includes/admin/class-wpglobus-language-edit.php:432
#: includes/options/fields/table/class-wpglobus-languages-table.php:105
msgid "Locale"
msgstr "منطقة اللغة"

#: includes/admin/class-wpglobus-language-edit.php:437
msgid "PHP/WordPress Locale of the language. (Examples: en_US, ru_RU)"
msgstr ""

#: includes/admin/class-wpglobus-language-edit.php:447
msgid "Save Changes"
msgstr "حفظ التغييرات"

#: includes/admin/class-wpglobus-language-edit.php:460
#: includes/admin/class-wpglobus-language-edit.php:466
msgid "Delete Language"
msgstr "مسح اللغة"

#: includes/admin/class-wpglobus-language-edit.php:474
msgid "Back to the WPGlobus Settings"
msgstr ""

#: includes/admin/class-wpglobus-plugin-install.php:306
msgid "Current Version"
msgstr "النسخة الحالية"

#: includes/admin/class-wpglobus-plugin-install.php:307
msgid "Get it now!"
msgstr "إحصل عليها الأن!"

#: includes/admin/class-wpglobus-plugin-install.php:308
msgid "Premium add-on"
msgstr ""

#: includes/admin/class-wpglobus-plugin-install.php:309
#: includes/options/fields/table/class-wpglobus-languages-table.php:476
msgid "Installed"
msgstr "مُثبت"

#: includes/admin/helpdesk/class-wpglobus-admin-helpdesk.php:54
msgid "Help Desk"
msgstr "طاولة المساعدة"

#: includes/admin/helpdesk/class-wpglobus-admin-helpdesk.php:55
msgid "Contact WPGlobus Support"
msgstr "تواصل مع دعم WPGlobus "

#: includes/admin/helpdesk/wpglobus-admin-helpdesk-page.php:55
msgid "Thank you for using WPGlobus!"
msgstr "شكراً لك لإستعمال WPGlobus!"

#: includes/admin/helpdesk/wpglobus-admin-helpdesk-page.php:56
msgid "Our Support Team is here to answer your questions or concerns."
msgstr ""

#: includes/admin/helpdesk/wpglobus-admin-helpdesk-page.php:58
msgid ""
"Click the blue round button at the bottom right to open the Contact Form."
msgstr ""

#: includes/admin/helpdesk/wpglobus-admin-helpdesk-page.php:59
msgid "Type in your name, email, subject and the detailed message."
msgstr ""

#: includes/admin/helpdesk/wpglobus-admin-helpdesk-page.php:60
msgid ""
"If you can make a screenshot demonstrating the problem, please attach it."
msgstr ""

#: includes/admin/helpdesk/wpglobus-admin-helpdesk-page.php:61
msgid ""
"Please note: we will receive some debug data together with your request. See "
"the \"Technical Information\" table for the details."
msgstr ""

#: includes/admin/helpdesk/wpglobus-admin-helpdesk-page.php:63
msgid "To help us serve you better:"
msgstr ""

#: includes/admin/helpdesk/wpglobus-admin-helpdesk-page.php:65
msgid ""
"Please check if the problem persists if you switch to a standard WordPress "
"theme."
msgstr ""

#: includes/admin/helpdesk/wpglobus-admin-helpdesk-page.php:66
msgid ""
"Try deactivating other plugins to see if any of them conflicts with WPGlobus."
msgstr ""

#: includes/admin/helpdesk/wpglobus-admin-helpdesk-page.php:73
msgid "Technical Information"
msgstr "المعلومات التقنية"

#: includes/admin/recommendations/class-wpglobus-admin-recommendations.php:54
msgid "WPGlobus Recommends:"
msgstr ""

#: includes/admin/recommendations/class-wpglobus-admin-recommendations.php:63
msgid "WPGlobus for WooCommerce"
msgstr ""

#: includes/admin/recommendations/class-wpglobus-admin-recommendations.php:67
msgid ""
"Translate product titles and descriptions, product categories, tags and "
"attributes."
msgstr ""

#: includes/admin/recommendations/class-wpglobus-admin-recommendations.php:72
msgid "Get it now:"
msgstr ""

#: includes/admin/recommendations/class-wpglobus-admin-recommendations.php:82
msgid "WPGlobus Multi-Currency"
msgstr ""

#: includes/admin/recommendations/class-wpglobus-admin-recommendations.php:86
msgid "Accept multiple currencies in your online store!"
msgstr ""

#: includes/admin/recommendations/class-wpglobus-admin-recommendations.php:91
#: includes/admin/recommendations/class-wpglobus-admin-recommendations.php:144
msgid "Check it out:"
msgstr ""

#: includes/admin/recommendations/class-wpglobus-admin-recommendations.php:142
msgid "Translate permalinks with our premium add-on, WPGlobus Plus!"
msgstr ""

#: includes/admin/recommendations/class-wpglobus-admin-recommendations.php:151
msgid "To translate permalinks, please activate the module Slug."
msgstr ""

#. / Do not translate
#: includes/admin/recommendations/class-wpglobus-admin-recommendations.php:154
msgid "Go to WPGlobus Plus Options page"
msgstr ""

#. / Do not translate
#: includes/class-wpglobus-filters.php:659
msgid "Error while saving."
msgstr "حصل خلل أثناء حفظ التغييرات."

#. / Do not translate
#: includes/class-wpglobus-filters.php:662
msgid "g:i:s a"
msgstr "g:i:s a"

#. / Do not translate
#: includes/class-wpglobus-filters.php:667
#, php-format
msgid "Draft saved at %s."
msgstr "تمّ حفظ المسودة - %s"

#. / Do not translate
#: includes/class-wpglobus-filters.php:766
msgid "&hellip;"
msgstr "&hellip;"

#. / Do not translate
#: includes/class-wpglobus-filters.php:771
msgctxt "word count: words or characters?"
msgid "words"
msgstr "كلمات"

#: includes/class-wpglobus-widget.php:27
msgid "WPGlobus widget"
msgstr ""

#: includes/class-wpglobus-widget.php:29
msgid "Add language switcher"
msgstr "أضف مبدل اللغة"

#: includes/class-wpglobus-widget.php:32
msgid "Flags"
msgstr "أعلام"

#: includes/class-wpglobus-widget.php:33
msgid "List"
msgstr "قائمة"

#: includes/class-wpglobus-widget.php:34
msgid "List with flags"
msgstr "قائمة بالأعلام"

#: includes/class-wpglobus-widget.php:35
msgid "Select"
msgstr "تحديد"

#: includes/class-wpglobus-widget.php:36
msgid "Select with language code"
msgstr "حدد كود اللغة"

#: includes/class-wpglobus-widget.php:37
msgid "Dropdown"
msgstr "القوائم المنسدلة"

#: includes/class-wpglobus-widget.php:38
msgid "Dropdown with flags"
msgstr ""

#: includes/class-wpglobus-widget.php:240
msgid "Selector type"
msgstr ""

#: includes/class-wpglobus.php:1157
msgid "You cannot disable the main language."
msgstr ""

#: includes/class-wpglobus.php:1359
msgid "*) Available after the menu is saved."
msgstr ""

#: includes/class-wpglobus.php:1376
msgid "Need a multilingual slug?"
msgstr ""

#. / Do not translate
#: includes/class-wpglobus.php:1452 includes/class-wpglobus.php:1456
#, php-format
msgid "You are customizing %s"
msgstr "أنت الآن تقوم بتخصيص %s"

#. / Do not translate
#: includes/class-wpglobus.php:2778
#, php-format
msgid "Word count: %s"
msgstr "عدد الكلمات: %s"

#. / Do not translate
#: includes/class-wpglobus.php:2788
#, php-format
msgid "Last edited by %1$s on %2$s at %3$s"
msgstr "آخر تحرير بواسطة  %1$s في يوم  %2$s الساعة  %3$s"

#. / Do not translate
#: includes/class-wpglobus.php:2796
#, php-format
msgid "Last edited on %1$s at %2$s"
msgstr "آخر تحرير في  %1$s في يوم  %2$s "

#. / Do not translate
#: includes/class-wpglobus.php:3097
msgid "Enter title here"
msgstr "أدخل العنوان هنا"

#. / Do not translate
#: includes/class-wpglobus.php:3219
msgid "Invalid taxonomy"
msgstr "فئة غير صالحة"

#: includes/class-wpglobus.php:3412
msgid "You must enable Pretty Permalinks to use WPGlobus."
msgstr ""

#: includes/class-wpglobus.php:3414
msgid ""
"Please go to Settings > Permalinks > Common Settings and choose a non-"
"default option."
msgstr ""

#: includes/class-wpglobus.php:3555
msgid "Add"
msgstr "إضافة"

#: includes/options/class-wpglobus-options.php:138
msgid "WARNING: old version of the ReduxFramework is active!"
msgstr ""

#: includes/options/class-wpglobus-options.php:141
#, php-format
msgid "WPGlobus settings panel requires ReduxFramework %1$s or later."
msgstr ""

#. / translators: ReduxFramework - %1$s version, %2$s folder where installed
#: includes/options/class-wpglobus-options.php:148
#, php-format
msgid ""
"The currently active ReduxFramework (version %1$s) was loaded from the %2$s "
"folder."
msgstr ""

#. / translators: %1$s placeholder for the link to ReduxFramework plugin
#: includes/options/class-wpglobus-options.php:157
#, php-format
msgid ""
"We recommend you to install the most recent version of the ReduxFramework "
"plugin: %1$s."
msgstr ""

#: includes/options/class-wpglobus-options.php:182
msgid ""
"Use the <strong>[Languages Table]</strong> section to add a new language or "
"to edit the language attributes: name, code, flag icon, etc."
msgstr ""

#: includes/options/class-wpglobus-options.php:210
msgid "Deactivating / Uninstalling"
msgstr ""

#: includes/options/class-wpglobus-options.php:244
msgid "Welcome!"
msgstr "مرحباً!"

#: includes/options/class-wpglobus-options.php:313
msgid "No navigation menu"
msgstr ""

#: includes/options/class-wpglobus-options.php:315
msgid "Select navigation menu"
msgstr ""

#. / translators: %s placeholder for the icon (actual picture)
#: includes/options/class-wpglobus-options.php:322
#, php-format
msgid ""
"Place the <strong>main language</strong> of your site at the top of the list "
"by dragging the %s icons."
msgstr ""

#: includes/options/class-wpglobus-options.php:323
msgid "<strong>Uncheck</strong> the languages you do not plan to use."
msgstr ""

#: includes/options/class-wpglobus-options.php:324
msgid "<strong>Add</strong> more languages using the section below."
msgstr ""

#: includes/options/class-wpglobus-options.php:325
msgid "When done, click the [Save Changes] button."
msgstr ""

#: includes/options/class-wpglobus-options.php:330
msgid ""
"Choose a language you would like to enable. <br>Press the [Save Changes] "
"button to confirm."
msgstr ""

#: includes/options/class-wpglobus-options.php:345
msgid "Instructions:"
msgstr "تعليمات:"

#: includes/options/class-wpglobus-options.php:346
#: includes/options/class-wpglobus-options.php:457
msgid "NOTE: you cannot remove the main language."
msgstr "ملحوظة: لا يمكنك مسح اللغة الرئيسية."

#: includes/options/class-wpglobus-options.php:368
msgid "Select a language"
msgstr "حدد لغة"

#: includes/options/class-wpglobus-options.php:410
msgid "(Found in some themes)"
msgstr ""

#: includes/options/class-wpglobus-options.php:415
msgid "Enable"
msgstr "مُفعّل"

#: includes/options/class-wpglobus-options.php:426
msgid "(Optional)"
msgstr "(إختياري)"

#: includes/options/class-wpglobus-options.php:450
msgid "Languages table"
msgstr "لائحة اللغات"

#: includes/options/class-wpglobus-options.php:456
msgid "Use this table to add, edit or delete languages."
msgstr "إستخدم هذه اللأئحة لكي تضيف،تعدل أو تزيل اللغات."

#: includes/options/class-wpglobus-options.php:702
msgid "Read the Quick Start Guide"
msgstr "إقرأ دليل البدء السريع"

#: includes/options/class-wpglobus-options.php:707
msgid "Visit our website"
msgstr "زر موقعنا الإلكتروني"

#: includes/options/class-wpglobus-options.php:712
msgid "Buy WooCommerce WPGlobus extension"
msgstr ""

#: includes/options/class-wpglobus-options.php:717
msgid "Collaborate on GitHub"
msgstr "ساعد في GitHub"

#: includes/options/class-wpglobus-options.php:723
msgid "Like us on Facebook"
msgstr "أعُجب بنا علي فيسبوك"

#: includes/options/class-wpglobus-options.php:728
msgid "Follow us on Twitter"
msgstr "إتبعنا علي تويتر"

#: includes/options/class-wpglobus-options.php:733
msgid "Find us on LinkedIn"
msgstr "جدنا علي LinkedIn"

#: includes/options/class-wpglobus-options.php:738
msgid "Circle us on Google+"
msgstr "أتبعنا علي جوجل بلس"

#: includes/options/fields/table/class-wpglobus-languages-table.php:56
msgid "item"
msgstr "عنصر"

#: includes/options/fields/table/class-wpglobus-languages-table.php:58
msgid "items"
msgstr "عناصر"

#: includes/options/fields/table/class-wpglobus-languages-table.php:78
msgid "Code"
msgstr "رمز"

#: includes/options/fields/table/class-wpglobus-languages-table.php:84
msgid "Edit"
msgstr "تعديل"

#: includes/options/fields/table/class-wpglobus-languages-table.php:89
msgid "Delete"
msgstr "محو"

#: includes/options/fields/table/class-wpglobus-languages-table.php:95
msgid "File"
msgstr "ملف"

#: includes/options/fields/table/class-wpglobus-languages-table.php:100
msgid "Flag"
msgstr "اشارة بعلم"

#: includes/options/fields/table/class-wpglobus-languages-table.php:110
msgid "Language name"
msgstr "أسم اللغة"

#: includes/options/fields/table/class-wpglobus-languages-table.php:115
msgid "English language name"
msgstr "أسم اللغة بالإنجليزية"

#: includes/options/fields/table/class-wpglobus-languages-table.php:141
msgid "No items found"
msgstr "لم يتم العثور على شيء"

#: includes/options/fields/table/class-wpglobus-languages-table.php:160
msgid "Add new Language"
msgstr "أضف لغة جديدة"

#: includes/options/fields/table/class-wpglobus-languages-table.php:403
msgid "Default language"
msgstr "اللغة الافتراضية"

#. / Do not translate
#: includes/options/fields/wpglobus_select/field_wpglobus_select.php:105
msgid "Select an item"
msgstr "اختر عنصر"

#. / Do not translate
#: includes/options/fields/wpglobus_select/field_wpglobus_select.php:161
msgid "No items of this type were found."
msgstr "لم يتم العثور على عناصر من هذا النوع."

#: includes/options/wpglobus-options-header.php:20
msgid "We rely on your support!"
msgstr "نحن نعتمد علي دعمك لنا!"

#: includes/options/wpglobus-options-header.php:23
msgid "Please consider a small donation to support the future development."
msgstr "من فضلك فكر في تبرع بنسبة بسيطة لدعم التطوير في المستقبل."

#: includes/options/wpglobus-options-header.php:27
msgid "Thank you!"
msgstr "شكراً لك!"

#: includes/options/wpglobus-options-header.php:48
msgid "WPGlobus Plus!"
msgstr ""

#: includes/options/wpglobus-options-header.php:51
msgid ""
"Advanced features and tweaks: URL translation, multilingual SEO analysis, "
"separate publishing and more! "
msgstr ""

#: includes/options/wpglobus-options-header.php:57
msgid "Get WPGlobus Plus now!"
msgstr "أحصل علي WPGlobus الأضافي الأن!"

#, fuzzy
#~ msgid "Custom JS code"
#~ msgstr "نمط مخصص"
