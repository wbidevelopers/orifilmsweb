						<div class="clear">
						</div>
					</div>
				</section>
			</div>
		</p>

		<?php 
			$id=10; 
			$post = get_post($id); 
			$content = apply_filters('the_content', $post->post_content); 
			echo $content;  
		?>
		
		<script type='text/javascript' src='<?php echo get_template_directory_uri();?>/js/plugins.js'></script>
		<script type='text/javascript' src='<?php echo get_template_directory_uri();?>/js/custom.js'></script>
		<script type='text/javascript' src='<?php echo get_template_directory_uri();?>/js/comment-reply.min.js'></script>
		<script type='text/javascript' src='<?php echo get_template_directory_uri();?>/js/wp-embed.min.js'></script>
		<script type='text/javascript' src='<?php echo get_template_directory_uri();?>/js/tides-video.js'></script>
		<?php wp_footer(); ?>
	</body>
</html>