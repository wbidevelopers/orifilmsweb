/*-----------------------------------------------------------------------------------*/
/*	MISC DOC READY .JS
/*-----------------------------------------------------------------------------------*/
jQuery(document).ready(function($){
'use strict';
	
	/**
	 * Disable parallax effects on mobile
	 */
	if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
		jQuery('body').addClass('mobile');
	} else {
		jQuery('body').addClass('desktop');
	}
	
	var iOS8 = navigator.userAgent.match(/(iPad|iPhone|iPod).*OS 8_\d/i);
	if( iOS8 ){
		setTimeout(function(){
			jQuery(window).trigger('load');
		}, 3500);
	}
	
	$(window).resize(function(){
		$('.full.ebor-full').css({ 'height' : $(window).height() - $('#main-header').outerHeight() });
	});
	
	$(window).trigger('resize');
	
	/**
	 * Hoverdir effects
	 */
	jQuery(function(){
		jQuery('.isotope-alt-image').each(function(){ 
			jQuery(this).hoverdir(); 
		});
	});
	
	jQuery('.accordion > dd').eq(0).addClass('active').show();
	jQuery('.accordion > dt').eq(0).addClass('active').show();
	  
	jQuery('.accordion > dt > a').click(function() {
		var $this = jQuery(this);
		if( $this.parent().hasClass('active') ){
			$this.parents('.accordion').find('dt').removeClass('active');
			$this.parents('.accordion').find('dd').removeClass('active').slideUp();
			return false;
		} else {
			$this.parents('.accordion').find('dt').removeClass('active');
			$this.parents('.accordion').find('dd').removeClass('active').slideUp();
			$this.parent().addClass('active').next().addClass('active').slideDown();
			return false;
		}
	});
	
	/**
	 * Remove empty p tags (shortcodes)
	 */
	jQuery('p:empty').remove();
	
	/**
	 * Mobile menu
	 */
	jQuery('#burger').click(function(){
		jQuery('#selectnav').slideToggle();
	});
	
	/**
	 * Downward arrow on menu items with children
	 */
	jQuery('#selectnav li a').parent().has('ul').append('<i class="fa fa-angle-down"></i>');
	
	/**
	 * Call fitvids for responsive video items
	 */
	jQuery('.video-container').fitVids();
	
	/**
	 * Alert Effects
	 */
	jQuery('.alert i').click(function(){
		jQuery(this).parent().slideUp();
	});

});
/*-----------------------------------------------------------------------------------*/
/*	ISOTOPE PORTFOLIO
/*-----------------------------------------------------------------------------------*/
jQuery(window).load(function($){
'use strict';
	
	var $isotope = jQuery('#isotope-portfolio');
	
	$isotope.isotope({
		itemSelector : 'li'
	});
	
	/*jQuery('#filters a').click(function(){
		var $this = jQuery(this);
		var filter = $this.attr('href');
		jQuery('#filters a').removeClass('active');
		$this.addClass('active');
		$isotope.isotope({ filter: filter });
		jQuery(window).trigger('resize');
		return false;
	});*/
	
	jQuery(window).smartresize(function(){
		$isotope.isotope('reLayout');
		setTimeout(function(){
			$isotope.isotope('reLayout');
		}, 501);
	});
	
	jQuery('.load-more').click(function(e){
		var $this = jQuery(this),
			url = jQuery(this).attr('href');
			
		$this.addClass('clicked');
		
		/*$this.html('<img src="' + wp_data.image_path + 'loader.gif" />');*/
		$this.html('<img src=../img/loader.gif" />');
		
		jQuery.get(url, function(data){
			var filtered = jQuery(data).find('#isotope-portfolio li');
			filtered.imagesLoaded(function(){
				filtered.find('.isotope-alt-image').each(function(){
					jQuery(this).hoverdir();
				});
				$isotope.isotope('insert', filtered).isotope('reLayout');
				$this.remove().next().css("display","block");
			});
		});
		
		e.stopImmediatePropagation();
		e.preventDefault();
	});
	
	jQuery(window).trigger('resize');
	
});
/*-----------------------------------------------------------------------------------*/
/*	SCROLL EFFECTS & HEADER
/*-----------------------------------------------------------------------------------*/
jQuery(window).load(function($){
'use strict';
	
	jQuery('.full.video, #big-video-wrap').animate({ 'opacity' : 1 });
	
	/**
	 * Page Loaded Stuff
	 */
	jQuery('#page-loader').addClass('animated fadeOutDown');
	jQuery('body, html').addClass('window-loaded');
	jQuery('.light.scroller').animate({ 'opacity' : '1' });
	
	/**
	 * Scroll Animations
	 */
	jQuery('.animated').espy(function (entered, state) {
		var $this = jQuery(this);
	    if (entered){
	    	$this.addClass( $this.attr('data-animation') );
	    }
	});
	
	// Bind the event.
	jQuery(window).hashchange( function(){
		var url = location.hash;
		var minus = 64;
		if( jQuery('body').hasClass('admin-bar') )
			var minus = 96;
			
		if( jQuery('body').hasClass('resize-off') )
			var minus = 108;
			
		if( jQuery('body').hasClass('resize-off') && jQuery('body').hasClass('admin-bar') )
			var minus = 140;
			
		if( '#home' == url )
			var minus = 200;
			
		if( url )
			jQuery("html, body").animate({ scrollTop: jQuery(url).offset().top - minus }, 500);
	});
	
	// Trigger the event (useful on page load).
	jQuery(window).hashchange();
	
	jQuery('#selectnav .scroller > a').click(function(){
		var $this = jQuery(this);
		jQuery('#selectnav .scroller').removeClass('active');
		$this.parent().addClass('active');
		history.pushState(null, null, $this.attr('href'));
		jQuery(window).hashchange();
		return false;
	});
	
	jQuery('a.scroller').click(function(){
		history.pushState(null, null, jQuery(this).attr('href'));
		jQuery(window).hashchange();
		return false;
	});
	
	jQuery(window).scroll(function(){
		
		if( jQuery('body').hasClass('resize-on') ){
			
			var scrollTop = jQuery(window).scrollTop() / 12;
			var $header = jQuery('header#main-header');
			var $sub = jQuery('#selectnav ul');
			
			if( scrollTop < 20 ){
				$header.css({
					'padding-top' : 25 - scrollTop, 
					'padding-bottom' : 25 - scrollTop
				});
				$sub.css({
					'padding-top' : 42 - scrollTop
				});
			} else {
				$header.css({
					'padding-top' : 5, 
					'padding-bottom' : 5
				});
				$sub.css({
					'padding-top' : 22
				});
			}
			
		}
		
		jQuery('#selectnav .scroller > a').each(function(){
			var scrollHref = jQuery(this).attr('href');
			if( jQuery(scrollHref).length > 0 ){
				if( jQuery(window).scrollTop() > jQuery(scrollHref).offset().top - 240 ) {
					jQuery('#selectnav .scroller').removeClass('active');
					jQuery(this).parent().addClass('active');
				}
			}
		});
		
	});

});
/*-----------------------------------------------------------------------------------*/
/*	PORTFOLIO
/*-----------------------------------------------------------------------------------*/
jQuery(document).ready(function($){
'use strict';
	
	/**
	 * Set details for owl carousel, that we can hook into later
	 */
	var defaults = {
		 // Most important owl features
		items : 5,
		itemsCustom : false,
		itemsDesktop : [1199,4],
		itemsDesktopSmall : [980,3],
		itemsTablet: [768,2],
		itemsTabletSmall: false,
		itemsMobile : [479,1],
		singleItem : true,
		itemsScaleUp : false,

		//Basic Speeds
		slideSpeed : 200,
		paginationSpeed : 800,
		rewindSpeed : 1000,

		//Autoplay
		autoPlay : false,
		stopOnHover : false,

		// Navigation
		navigation : true,
		navigationText : ["<i class=\"fa fa-chevron-left\"></i>","<i class=\"fa fa-chevron-right\"></i>"],
		rewindNav : true,
		scrollPerPage : false,

		//Pagination
		pagination : true,
		paginationNumbers: false,

		// Responsive
		responsive: true,
		responsiveRefreshRate : 200,
		responsiveBaseWidth: window,

		// CSS Styles
		baseClass : "owl-carousel",
		theme : "owl-theme",

		//Lazy load
		lazyLoad : false,
		lazyFollow : true,
		lazyEffect : "fade",

		//Auto height
		autoHeight : true,

		//JSON
		jsonPath : false,
		jsonSuccess : false,

		//Mouse Events
		dragBeforeAnimFinish : true,
		mouseDrag : true,
		touchDrag : true,

		//Transitions
		transitionStyle : false,

		// Other
		addClassActive : false,

		//Callbacks
		beforeUpdate : false,
		afterUpdate : false,
		beforeInit: false,
		afterInit: false,
		beforeMove: false,
		afterMove: false,
		afterAction: false,
		startDragging : false,
		afterLazyLoad : false
	}
	
	/**
	 * Call owl carousel on each instance of the plugin, grab set up from the HTML
	 */
	jQuery(".owl-carousel").each(function(options){
		var slider = jQuery(this);
		var config = jQuery.extend({}, defaults, options, slider.data("owl"));
		slider.owlCarousel(config);			
	});
	
	/**
	 * AJAX portfolio item loading
	 */		
	jQuery('body').on('click', '#isotope-portfolio a.ajax-link', function(){
		
		jQuery('#page-loader.animated').animate({ 'opacity' : '1'}, 'slow');
		
		var url = jQuery(this).attr('href'),
			portfolioHeight = jQuery('#isotope-portfolio').height();
		
		jQuery("html, body").animate({ scrollTop: jQuery('#isotope-portfolio').offset().top - 330 }, 500);
		
		jQuery('#filters').removeClass('fadeInDown').addClass('fadeOutUp').delay(1000).hide(1);
		
		jQuery('#isotope-portfolio, #load-more-wrapper').addClass('fadeOutLeft').delay(1000).hide(1, function(){
			jQuery(this).fadeOut();
			jQuery('#loader').css('min-height', portfolioHeight).fadeIn();
		});
		
		jQuery.get(url, function(data){
			var filtered = jQuery(data).find('article');
			filtered.imagesLoaded(function(){
				
				jQuery(".owl-carousel", filtered).each(function(options){
					var slider = jQuery(this);
					var config = jQuery.extend({}, defaults, options, slider.data("owl"));
					slider.owlCarousel(config);			
				});
				
				jQuery('.video-container', filtered).fitVids();
				
				jQuery('.animated', filtered).addClass( jQuery(this).attr('data-animation') );
					
				jQuery('#loader').html(filtered).removeClass('fadeOutDown').show(1).addClass('fadeInUp').find('.animated').addClass( jQuery(this).attr('data-animation') );
				
				jQuery('#page-loader.animated').animate({ 'opacity' : '0'}, 'slow');
			});
		});
			
		return false;
	});
	
	/**
	 * Close the portfolio modal
	 */
	jQuery('body').on('click', '.portfolio-close', function(){
	
		jQuery('#loader').removeClass('fadeInUp').addClass('fadeOutDown').delay(999).hide(1);
		setTimeout(function(){
			jQuery('#loader').html(' ');
		}, 999);
		jQuery('#isotope-portfolio, #load-more-wrapper').delay(1000).slideDown(function(){
			jQuery('#isotope-portfolio').isotope('reLayout');
		}).removeClass('fadeOutLeft').addClass('fadeInRight');
		jQuery('#filters').delay(999).show(1).removeClass('fadeOutUp').addClass('fadeInDown');
		
		return false;
	});
	
});