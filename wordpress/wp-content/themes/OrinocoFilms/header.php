<!DOCTYPE html>
<!--[if IE 9]> <html class="js ie9 oldie" lang="en-US"> <![endif]-->
<!--[if gt IE 8]><!--> 
<html class="js" <?php language_attributes(); ?> >
<!--<![endif]-->

	<head>
		<meta charset="UTF-8">
		<title><?php wp_title(''); ?></title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">		
		<link rel='stylesheet' href='<?php echo get_template_directory_uri();?>/css/aqpb-view.css' type='text/css' media='all' />
		<link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
		<link rel='stylesheet' href='<?php echo get_template_directory_uri();?>/css/font-awesome.min.css' type='text/css' media='all' />
		<link rel='stylesheet' href='<?php echo get_template_directory_uri();?>/css/orinoco.css' type='text/css' media='all' />
		<link rel='stylesheet' href='<?php echo get_template_directory_uri();?>/css/framework.css' type='text/css' media='all' />
		<link rel='stylesheet' href='<?php echo get_template_directory_uri();?>/css/plugins.css' type='text/css' media='all' />
		<link rel='stylesheet' href='<?php echo get_template_directory_uri();?>/style.css' type='text/css' media='all' />
		<link rel='stylesheet' href='<?php echo get_template_directory_uri();?>/custom.css' type='text/css' media='all' />
		<script type='text/javascript' src='<?php echo get_template_directory_uri();?>/js/jquery.js'></script>
		<script type='text/javascript' src='<?php echo get_template_directory_uri();?>/js/jquery-migrate.min.js'></script>
		<?php wp_head(); ?>
	</head>


<body class="home page page-id-8 page-template page-template-page_home page-template-page_home-php resize-on">
	
	<div id="page-loader">
		<img src="<?php echo get_template_directory_uri();?>/img/loader.gif" alt="loader" />
	</div>
	
	<header id="main-header">
		<div class="wrapper clearfix">
			<div id="burger"><i class="fa fa-bars"></i></div>
			<div class="one_fourth remove_bottom logo">
				<a href="<?php echo get_home_url(); ?>">
					<img src="<?php echo get_template_directory_uri();?>/img/logo.png" alt="Alt Text" class="retina" />
				</a>	
			</div><!--/one_fourth-->
			<div class="three_fourths remove_bottom last">
				<nav id="site-navigation">
					<?php 
						if ( class_exists( 'WPGlobus' ) ) {
							foreach( WPGlobus::Config()->enabled_languages as $lang ) {

								if ( $lang == WPGlobus::Config()->language ) {
									continue;
								}
								$flag = WPGlobus::Config()->flags_url . WPGlobus::Config()->flag[ $lang ];
		                        $args = array(
		                            'theme_location' => 'HeaderMenu',
		                            'menu_class' => 'menu-main-menu-container',
		                            'menu_id' => 'site-navigation',
		                            'items_wrap' => '<ul id="selectnav" class="menu">%3$s<li><a class="lang" href="' . WPGlobus_Utils::localize_current_url( $lang ). '"><img src="' . $flag . '" alt="flag" /></a></li></ul>'
		                        );
		                        wp_nav_menu($args);
							}
						}

                    ?>		
				</nav>
			</div><!--/three_fourths-->
		</div><!--/wrapper-->
	</header><!--/header-->
	
	<div class="offset clear"></div>