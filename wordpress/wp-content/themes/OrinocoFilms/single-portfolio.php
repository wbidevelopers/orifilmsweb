<?php 
/*
 * Template Name: Portfolio Layout
 * Template Post Type: portfolio
 */
?> 

    <?php
        // Start the loop.
        while ( have_posts() ) : the_post();
    ?>

    <section class="dark-wrapper">
        <div class="wrapper">         
            <article id="portfolio-84" class="animated post-84 portfolio type-portfolio status-publish format-image has-post-thumbnail hentry portfolio-category-brochures portfolio-category-web-design fadeInUp animated-visible" data-animation="fadeInUp animated-visible">
                <a href="#" class="portfolio-close"><i class="fa fa-times"></i></a>
                    <?php
                        $post_format = get_post_format();
                        if ($post_format == "video"){
                        ?>
                            <!-- VIDEO -->
                            <div class="videoWrapper animated format-image-wrapper fadeInUp animated-visible" data-animation="fadeInUp animated-visible">
                                <iframe src="<?php echo get_field( "video" ); ?>" frameborder="0" allowfullscreen="" name="fitvid0"></iframe>  
                            </div> 
                            <?php
                        } elseif ($post_format == "image") {
                            ?>
                            <!-- IMAGE -->
                            <div class="animated format-image-wrapper fadeInUp animated-visible" data-animation="fadeInUp animated-visible">
                                <?php echo get_the_post_thumbnail($post_id, 'medium'); ?>      
                            </div> 
                            <?php
                        } elseif ($post_format == "gallery") {
                            ?>
                            <!-- GALERIA -->
                            <div class="animated format-image-wrapper fadeInUp animated-visible" data-animation="fadeInUp animated-visible">
                                <?php echo do_shortcode(get_field( "galeria" )); ?>
                            </div> 
                            <?php
                        } else {
                            ?>
                            <!-- OTRO -->
                            <div class="animated format-image-wrapper fadeInUp animated-visible" data-animation="fadeInUp animated-visible">
                                <?php echo get_the_post_thumbnail($post_id, 'medium'); ?>      
                            </div> 
                            <?php
                        }
                    ?>       
                    <div class="content clearfix">
                        <div class="two_thirds justify">
                            <h4><?php echo the_title(); ?></h4>
                            <?php echo the_content(); ?>
                            <div class="clear"></div>
                        </div>
                        <div class="one_third last">
                            <?php 
                                if (WPGlobus::Config()->language == "ve"){
                                    echo '<h5>Detalles</h5>';
                                }else{
                                    echo '<h5>Details</h5>';
                                }
                            ?>
                            <p> 
                                <?php
                                    if (WPGlobus::Config()->language == "ve"){
                                        $field = get_field("trailer");
                                        if ($field) {
                                            echo '<a id="trailer" href="'.$field.'" target="_blank"><strong>VER TRAILER</strong></a><br>';
                                        }  
                                        $field = '';
                                        $field = get_field("countryear");
                                        if ($field) {
                                            echo $field.'<br>';
                                        }  
                                        $field = '';
                                        $field = get_field("topic");
                                        if ($field) {
                                            echo $field.'<br>';
                                        }  
                                        $field = '';
                                        $field = get_field("protagonistas");
                                        if ($field) {
                                            echo '<strong>Protagonistas: </strong>'. $field .'<br>';
                                        }  
                                        $field = '';
                                        $field = get_field("distribucion");
                                        if ($field) {
                                            echo '<strong>Distribución: </strong>'. $field.'<br>';
                                        }  
                                        $field = '';
                                        $field = get_field("idioma");
                                        if ($field) {
                                            echo '<strong>Idioma: </strong>'. $field .'<br>';
                                        }  
                                        $field = '';
                                        $field = get_field("direccion");
                                        if ($field) {
                                            echo '<strong>Dirección: </strong>'. $field .'<br>';
                                        }  
                                        $field = '';
                                        $field = get_field("guion");
                                        if ($field) {
                                            echo '<strong>Guión: </strong>'. $field .'<br>';
                                        }  
                                        $field = '';
                                        $field = get_field("produccion");
                                        if ($field) {
                                            echo '<strong>Producción: </strong>'. $field .'<br>';
                                        }  
                                        $field = '';
                                        $field = get_field("musica");
                                        if ($field) {
                                            echo '<strong>Música: </strong>'. $field .'<br>';
                                        }  
                                        $field = '';
                                        $field = get_field("voces_originales");
                                        if ($field) {
                                            echo '<strong>Voces Originales: </strong>'. $field .'<br>';
                                        }  
                                        $field = '';
                                        $field = get_field("productoras");
                                        if ($field) {
                                            echo '<strong>Productoras: </strong>'. $field .'<br>';
                                        }  
                                        $field = '';
                                        $field = get_field("web");
                                        if ($field) {
                                            echo '<strong>Website: </strong><a href="'. $field .'" target="_blank" class="light">'. $field .'<br>';
                                        }  
                                        $field = '';
                                    }else{
                                        $field = get_field("trailer");
                                        if ($field) {
                                            echo '<a id="trailer" href="'.$field.'" target="_blank"><strong>WATCH THE TRAILER</strong></a><br>';
                                        }  
                                        $field = '';
                                        $field = get_field("countryear");
                                        if ($field) {
                                            echo $field.'<br>';
                                        }  
                                        $field = '';
                                        $field = get_field("topic");
                                        if ($field) {
                                            echo $field.'<br>';
                                        }  
                                        $field = '';
                                        $field = get_field("protagonistas");
                                        if ($field) {
                                            echo '<strong>Cast: </strong>'. $field .'<br>';
                                        }  
                                        $field = '';
                                        $field = get_field("distribucion");
                                        if ($field) {
                                            echo '<strong>Distribution companies: </strong>'. $field.'<br>';
                                        }  
                                        $field = '';
                                        $field = get_field("idioma");
                                        if ($field) {
                                            echo '<strong>Language: </strong>'. $field .'<br>';
                                        }  
                                        $field = '';
                                        $field = get_field("direccion");
                                        if ($field) {
                                            echo '<strong>Directed by: </strong>'. $field .'<br>';
                                        }  
                                        $field = '';
                                        $field = get_field("guion");
                                        if ($field) {
                                            echo '<strong>Screenplay: </strong>'. $field .'<br>';
                                        }  
                                        $field = '';
                                        $field = get_field("produccion");
                                        if ($field) {
                                            echo '<strong>Production companies: </strong>'. $field .'<br>';
                                        }  
                                        $field = '';
                                        $field = get_field("musica");
                                        if ($field) {
                                            echo '<strong>Music: </strong>'. $field .'<br>';
                                        }  
                                        $field = '';
                                        $field = get_field("voces_originales");
                                        if ($field) {
                                            echo '<strong>Original Voices: </strong>'. $field .'<br>';
                                        }  
                                        $field = '';
                                        $field = get_field("productoras");
                                        if ($field) {
                                            echo '<strong>Producers: </strong>'. $field .'<br>';
                                        }  
                                        $field = '';
                                        $field = get_field("web");
                                        if ($field) {
                                            echo '<strong>Website: </strong><a href="'. $field .'" target="_blank" class="light">'. $field .'<br>';
                                        }  
                                        $field = '';
                                    }
                                    
                                ?>
                            </p>
                        </div>
                    </div>
                </article>
            </div>
        </section>

    <?php
        // End of the loop.
        endwhile;
    ?>