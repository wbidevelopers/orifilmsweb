<?php 
/*
 * Template Name: Post Layout
 * Template Post Type: post
 */
get_header();
?> 

    <?php
        // Start the loop.
        while ( have_posts() ) : the_post();
    ?>

    <section class="dark-wrapper">
        <div class="wrapper">
            <div class="one_third animated fadeInUp animated-visible" data-animation="fadeInUp animated-visible">
                <?php echo get_the_post_thumbnail($post_id, 'medium'); ?>
            </div>
            <div class="two_thirds last justify">
                <article id="team-93" class="animated post-93 team type-team status-publish has-post-thumbnail hentry fadeInUp animated-visible" data-animation="fadeInUp animated-visible">
                    <div class="content clearfix">
                        <h3 class="entry-title"><?php echo the_title(); ?></h3>
                        <?php echo the_content(); ?>
                    </div>
                </article>
            </div>
            <div class="clear"></div>
        </div>
    </section>

    <?php
        // End of the loop.
        endwhile;
    ?>

<?php get_footer(); ?>