<?php
/*
 * Template Name: Home/Inicio
 * Description: Main page template
 */
get_header();
?>

<div id="aq-template-wrapper-21" class="aq-template-wrapper aq_row">
	<div id="aq-block-21-1" class="aq-block aq-block-aq_video_block aq_span12 aq-first clearfix">	
		<?php echo do_shortcode('[rev_slider alias="home"]'); ?>
	</div>
</div>

<div id="aq-template-wrapper-14" class="aq-template-wrapper aq_row">
	<!-- QUIENES SOMOS -->		
	<section class="quienes-small-padding">
		<div class="wrapper centered">
			<?php 
				$id=22; 
				$post = get_post($id); 
				$content = apply_filters('the_content', $post->post_content);
				$title = apply_filters('the_title', $post->post_title);
			?>
			<h3 class="centered">
				<?php echo $title; ?>
			</h3>
			<div class="wrapper centered">
				<div class="one_fourth"></div>
				<div class="one_half"><?php echo $content;?></div>
				<div class="one_fourth"></div>
			</div>
			<div class="clear"></div><!--CLEAR FLOATS-->
		</div>
	</section>	
	<!-- //QUIENES SOMOS -->		
	<!-- SERVICIOS -->	
	<div class="section-marker" id="servicios">
	</div>
	<section class="services-small-padding">
		<div class="wrapper centered">
			<h3 class="iblock">
				<?php 
                    if (WPGlobus::Config()->language == "ve"){
						echo 'Nuestros Servicios<br><small>¡Listos para lo mejor!</small>';
                    }else{
                    	echo 'Our Services<br><small>Prepare for Awesome!</small>';
                    }
                ?>
			</h3>
			<div class="clear"></div><!--CLEAR FLOATS-->
		</div>
	</section>
	<section id="sec-servicios" class="dark-wrapper">
		<div class="wrapper">
			<?php
				$args = array( 
					'numberposts' => -1,
					'category_name' => 'servicios',
					'orderby' => 'post_date',
					'order' => 'ASC'
				);
				$i = 1;
				$servicios = new WP_Query( $args );
				if ( $servicios->have_posts() ) : 
					while ( $servicios->have_posts() ) : $servicios->the_post();
						if (($i == 1) or ($i == 4) or ($i == 7)):
						?>
							<div id="aq-block-<?php echo $i ?>" class="aq-block aq-block-aq_icon_column_block aq_span4 aq-first clearfix">	
								<?php echo the_content(); ?>
							</div>
							<?php 
						$i++;
						else: 
						?>
							<div id="aq-block-<?php echo $i ?>" class="aq-block aq-block-aq_icon_column_block aq_span4 clearfix">	
								<?php echo the_content(); ?>
							</div>
							<?php
						$i++;
						endif;
					endwhile;
                endif;
                wp_reset_postdata();
			?>
			<div class="clear"></div>
		</div>
	</section>
	<section id="sec-serepecial" class="dark-wrapper">
		<div class="wrapper">
			<div id="aq-block-14-12" class="aq-block aq-block-aq_team_block aq_span12 aq-first clearfix">		
				<ul class="clearfix portfolio-isotope animated animated-visible fadeInUp" data-animation="animated-visible fadeInUp">
					<?php
						$args = array( 
							'numberposts' => -1,
							'category_name' => 'servicios_especiales',
							'orderby' => 'post_date',
							'order' => 'ASC'
						);
						$i = 1;
						$servicios_esp = new WP_Query( $args );
						if ( $servicios_esp->have_posts() ) : 
							while ( $servicios_esp->have_posts() ) : $servicios_esp->the_post();
								if (($i == 1) or ($i == 4) or ($i == 7)):
								?>
									<li id="team-<?php echo $i ?>" class="member post-93 team type-team status-publish has-post-thumbnail hentry">
										<a href="<?php echo get_the_permalink(); ?>" class="isotope-alt-image">
											<?php echo get_the_post_thumbnail($post_id, 'medium_large'); ?>		
											<div class="sec-a">
												<h4><?php echo the_title(); ?></h4>		
											</div>	
										</a>
										<div class="isotope-alt-details">
											<div>
												<h4 class="remove-bottom"><?php echo the_title(); ?></p>			
											</div>
										</div>
									</li>
									<?php 
								$i++;
								else: 
								?>
									<li id="team-<?php echo $i ?>" class="member post-92 team type-team status-publish has-post-thumbnail hentry">
										<a href="<?php echo get_the_permalink(); ?>" class="isotope-alt-image">
											<?php echo get_the_post_thumbnail($post_id, 'medium_large'); ?>		
											<div class="sec-b">
												<h4><?php echo the_title(); ?></h4>
											</div>
										</a>
										<div class="isotope-alt-details">
											<div>
												<h4 class="remove-bottom"><?php echo the_title(); ?></p>			
											</div>
										</div>
									</li>
									<?php
								$i++;
								endif;
							endwhile;
		                endif;
		                wp_reset_postdata();
					?>
				</ul>
			</div>
			<div class="clear"></div>
		</div>
	</section>
	<!-- //SERVICIOS -->	
	<!-- DEMO REEL -->
	<div class="section-marker" id="demoReel">
	</div>
	<section class="demoreel-small-padding">
		<div class="wrapper centered">
			<h3 class="iblock">
				<?php 
                    if (WPGlobus::Config()->language == "ve"){
						echo 'Demo Reel<br><small>Una vista rápida a nuestro trabajo</small>';
                    }else{
                    	echo 'Demo Reel<br><small>A quick look to our work</small>';
                    }
                ?>
			</h3>
			<div class="clear"></div><!--CLEAR FLOATS-->
		</div>
	</section>
	<section class="demoreel-dark-wrapper">
		<div class="wrapper">
			<div id="aq-block-14-4" class="centered clearfix">	
				<?php 
					$id=24; 
					$post = get_post($id); 
					$content = apply_filters('the_content', $post->post_content); 
					echo $content;  
				?>
			</div>
			<div class="clear"></div>
		</div>
	</section>
	<!-- //DEMO REEL -->
	<!-- CATALOGO -->
	<div class="section-marker" id="catalogo">
	</div>
	<section class="catalog-small-padding">
		<div class="wrapper centered">
			<h3 class="iblock">
				<?php 
                    if (WPGlobus::Config()->language == "ve"){
						echo 'Nuestro Trabajo<br><small>¡Totalmente orgullosos!</small>';
                    }else{
                    	echo 'Our Work<br><small>We&#39;re Really Proud!</small>';
                    }
                ?>
			</h3>
			<div class="clear"></div><!--CLEAR FLOATS-->
		</div>
	</section>
	<section class="dark-wrapper">
		<div class="wrapper">
			<div id="aq-block-14-2" class="aq-block aq-block-aq_portfolio_block aq_span12 aq-first clearfix">
				<div id="filters" class="animated hideall" data-animation="fadeInDown animated-visible">
					<a href="*" class="active button">Todos</a>
				</div>		
				<div id="loader" class="animated">
				</div>
				<ul id="isotope-portfolio" class="clearfix portfolio-isotope animated" data-animation="animated-visible fadeInUp">
					<?php
						// Fix for pagination
					    if( is_front_page() ) { 
					      $paged = ( get_query_var( 'page' ) ) ? get_query_var( 'page' ) : 1; 
					    } else { 
					      $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1; 
					    }
						$args = array( 
								'post_type' => 'portfolio',
								'orderby' => 'post_date',
								'order' => 'DESC',
								'posts_per_page' => 6,
	                            'paged' => $paged
							 );
						//$projects = get_posts($args);
						$projects = new WP_Query( $args );
						if ( $projects->have_posts() ) : while ( $projects->have_posts() ) : $projects->the_post();
						?>
							<li id="portfolio-<?php echo get_the_ID(); ?>">
								<a href="<?php echo get_the_permalink($project->ID); ?>" class="ajax-link isotope-alt-image">
									<?php echo get_the_post_thumbnail($post_id, 'medium', array('class'=>'attachment-portfolio size-portfolio wp-post-image')); ?>		
									<div>
										<h4><?php echo the_title(); ?></h4>		
									</div>
								</a>
								<div class="isotope-alt-details">
									<div>
										<h4 class="remove-bottom"><?php echo the_title(); ?></h4>	
									</div>
								</div>
							</li>
							<?php
						endwhile;
					?>
				</ul>
				<div id='load-more-wrapper' class='animated' data-animation='animated-visible fadeInUp'>
					<?php 
						if (WPGlobus::Config()->language == "ve"){
							next_posts_link( "Ver más" , $projects->max_num_pages );
						}else{
							next_posts_link( "Load more" , $projects->max_num_pages );
						}
					?>
				</div>
				<?php
                endif;
                wp_reset_query();
			?>
			</div>
			<div class="clear"></div>
		</div>
	</section>	
	<!-- //CATALOGO -->
	<!-- CLIENTES -->
	<div class="section-marker" id="clientes"></div>
	<section class="clients-small-padding">
		<div class="wrapper centered">
			<h3 class="iblock">
				<?php 
					if (WPGlobus::Config()->language == "ve"){
						echo 'Nuestros Clientes<br><small>¡Personas felices!</small>';
					}else{
						echo 'Our Clients<br><small>Happy people!</small>';
					}
				?>
			</h3>
			<div class="clear"></div><!--CLEAR FLOATS-->
		</div>
	</section>			
	<section class="parallax">
		<div class="wrapper">
			<div id="aq-block-14-9" class="aq-block aq-block-aq_image_carousel_block aq_span12 aq-first clearfix">		
				<div class="animated" data-animation="fadeInUp animated-visible">
					<div class="owl-carousel testimonials" data-owl='{"items":4, "singleItem": false}'>
						<?php
							$args = array( 
								'numberposts' => -1,
								'category_name' => 'clientes',
								'orderby' => 'post_date',
								'order' => 'ASC'
							 );
						    $posts = get_posts($args);
						    foreach($posts as $post) : setup_postdata($post);
							?>
								<div>
								  	<?php echo get_the_post_thumbnail($post_id, 'medium_large'); ?>
								</div>
								<?php
							endforeach; 
						wp_reset_query();
					    ?>
	  				</div>
				</div>
			</div>
			<div class="clear">
			</div>
		</div>
	</section>	
	<!-- //CLIENTES -->	
</div>	
<?php get_footer(); ?>